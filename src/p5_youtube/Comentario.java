package p5_youtube;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Comentario {

	private String usuario;
	private String contenido;
	private Date fechaComentario;

	public Comentario(String contenido, String usuarioID) {

		setUsuario(usuarioID);
		setContenidoComentario(contenido);
		setFechaComentario(fechaComentario);
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuarioID) {
		this.usuario = usuarioID;
	}

	public String getContenidoComentario() {
		return contenido;
	}

	public void setContenidoComentario(String contenido) {
		this.contenido = contenido;
	}

	public Date getFechaComentario() {
		return fechaComentario;
	}

	public void setFechaComentario(Date fechaComentario) {
		this.fechaComentario = fechaComentario;
	}

	public String formatoFecha() {
		return (new SimpleDateFormat("dd-MM-yyyy").format(getFechaComentario()));
	}
}
