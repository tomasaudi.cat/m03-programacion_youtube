package p5_youtube;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Video {

	private String nombreVideo;
	private Date fecha = new Date();
	ArrayList<Comentario> comentarios;
	private int likes;

	public Video(String nombreVideo) {
		this.setNombreVideo(nombreVideo);
		this.setFechaCreacion(fecha);
		this.setLikes(likes);
		comentarios = new ArrayList<Comentario>();
	}
	
	// GETTERS & SETTERS
	public String getNombreVideo() {
		return nombreVideo;
	}

	public void setNombreVideo(String nombreVideo) {
		this.nombreVideo = nombreVideo;
	}

	public Date getFechaCreacion() {
		return fecha;
	}

	public void setFechaCreacion(Date fecha) {
		this.fecha = fecha;
	}

	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public String Formatofecha() {
		return (new SimpleDateFormat("dd-MM-yyyy").format(getFechaCreacion()));
	}

	// FUNCION MENU VIDEO
	public void getMenuVideo() {
		Scanner leer = new Scanner(System.in);

		int opcion;
		do {
			mostrarMenuVideo();
			opcion = leer.nextInt();

			switch (opcion) {

			// OPCION 1 - CREAR CANAL
			case 1:
				nuevoComentario();
				break;

			// OPCION 2 - SELECIONAR CANAL
			case 2:
				like();
				break;

			// OPCION 3 - MOSTRAR ESTADISTICAS
			case 3:
				mostrarComentario();
				break;

			// OPCION 4 - MOSTRAR ESTADISTICAS COMPLETAS
			case 4:
				mostrarEstadisticas();
				break;

			// OPCION 0 - SALIR DEL PROGRAMA
			case 0:
				salir();
				System.out.println("*SALIENDO*\n");
				break;

			// OPCION INCORRECTA
			default:
				System.out.println("*Opción no válida* \nIntroduce una opción válida.");
			}
		} while (opcion != 0);
	}

	// FUNCION CONTENIDO MENU
	public void mostrarMenuVideo() {
		System.out.println("--" + getNombreVideo() + "--");
		System.out.println("1- Nuevo Comentario");
		System.out.println("2- Like");
		System.out.println("3- Mostrar Comentarios");
		System.out.println("4- Mostrar Estadisticas");
		System.out.println("0- Salir");
		System.out.println("____________________________");
	}

	// FUNCION LIKES
	public void like() {
		likes = likes + 1;
		System.out.println("Le has dado 'Like' al video.\n");
	}

	// FUNCION CREAR UN NUEVO COMENTARIO
	public void nuevoComentario() {
		Scanner leer = new Scanner(System.in);
		// DECLARAMOS VARIABLES
		String comentarioTexto;
		String usuarioID;
		// PEDIMOS VALORES DE LAS VARIABLES
		System.out.println("Introduce tu nombre de usuario: ");
		usuarioID = leer.nextLine();
		System.out.println("Escribe un comentario: ");
		comentarioTexto = leer.nextLine();
		// AÑADIMOS AMBAS VARIABLES EN EL ARRAYLIST DE COMENTARIOS
		comentarios.add(new Comentario(comentarioTexto, usuarioID));
	}

	// FUNCION MOSTRAR COMENTARIOS
	public void mostrarComentario() {
		// DECLARAMOS VARIABLE
		Comentario comentario;
		// BUCLE FOR PARA RECORRER EL ARRAY COMENTARIOS Y MOSTRAR CONTENIDO
		System.out.println("--COMENTARIOS--");
		for (int i = 0; i < comentarios.size(); i++) {
			comentario = comentarios.get(i);
			System.out.println(" -" + comentario.getUsuario() + ": " + comentario.getContenidoComentario());
		}
		System.out.println("-----------------------------");
	}

	// FUNCION MOSTRAR ESTADISTICAS DEL VIDEO
	public void mostrarEstadisticas() {
		System.out.println("El video tiene: " + comentarios.size() + " comentarios, y " + likes + " likes.");
	}

	// FUNCION SALIR MENU VIDEO A CANAL
	public void salir() {
	}

}
