package p5_youtube;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Canal {

	private String nombreCanal;
	private Date fecha = new Date();
	ArrayList<Video> videos;
	Scanner leer = new Scanner(System.in);

	public Canal(String Canal) {
		this.setnombreCanal(Canal);
		this.setFechaCreacion(fecha);
		videos = new ArrayList<Video>();

	}

	// GETTERS & SETTERS
	public String getnombreCanal() {
		return nombreCanal;
	}

	public void setnombreCanal(String nombreCanal) {
		this.nombreCanal = nombreCanal;
	}

	public Date getFechaCreacion() {
		return fecha;
	}

	public void setFechaCreacion(Date fecha) {
		this.fecha = fecha;
	}

	public String formatoFecha() {
		return (new SimpleDateFormat("dd-MM-yyyy").format(getFechaCreacion()));
	}

	// FUNCION MENU CANAL
	public void getMenuCanal() {
		int opcion;
		do {
			mostrarMenuCanal();
			opcion = Integer.parseInt(leer.nextLine());

			switch (opcion) {
			// OPCION 1 - CREAR UN NUEVO VIDEO
			case 1:
				nuevoVideo();
				break;

			// OPCION 2 - SELECCIONAR UN VIDEO
			case 2:
				seleccionarVideo();
				break;

			// OPCION 3 - MOSTRAR LAS ESTADISTICAS DEL VIDEO
			case 3:
				mostrarEstadisticas();
				break;

			// OPCION 4 - MOSTRAR INFORMACION DEL CANAL
			case 4:
				mostrarInfo();
				break;

			// OPCION 5 - SALIR DEL MENU CANAL
			case 0:
				salir();
				break;

			default:
				System.out.println("*Opción no válida* \nIntroduce una opción válida.");
			}
		} while (opcion != 0);

	}

	// FUNCION CONTENIDO DEL MENÚ
	public void mostrarMenuCanal() {
		System.out.println("--" + getnombreCanal() + "--");
		System.out.println("1- Nuevo Video");
		System.out.println("2- Seleccionar Video");
		System.out.println("3- Mostrar Estadisticas");
		System.out.println("4- Mostrar información videos");
		System.out.println("0- Salir");
		System.out.println("-----------------------------");
	}

	// FUNCION CREAR NUEVO VIDEO
	public void nuevoVideo() {
		Scanner leer = new Scanner(System.in);
		String nombreVideo;
		System.out.println("Nombre del video: ");
		nombreVideo = leer.nextLine();
		Video nuevoVideo = new Video(nombreVideo);
		videos.add(nuevoVideo);
		nuevoVideo.getMenuVideo();
		

	}

	// FUNCION SELECIONAR VIDEO
	public void seleccionarVideo() {
		if (videos.isEmpty()) {
			System.out.println("No hay videos disponibles.");
		} else {
			System.out.println("Seleccione un video: ");
			Video video;
			for (int i = 0; i < videos.size(); i++) {
				video = videos.get(i);
				System.out.println(i + "- Nombre del video: " + "'" + video.getNombreVideo() + "'"
						+ " creado en la fecha: " + video.Formatofecha());
			}

			int indice = leer.nextInt();
			leer.nextLine();

			if (indice >= 0 && indice < videos.size()) {

				video = videos.get(indice);
				video.getMenuVideo();

			} else {
				System.out.println("Opción no válida.");
			}
		}
	}

	// FUNCION MOSTRAR INFORMACION DEL CANAL
	public void mostrarInfo() {
		
		System.out.println("Nombre del canal: " + nombreCanal + " en fecha: " + formatoFecha() + " con " + videos.size() + " videos." );
	}
	
	// FUNCION MOSTRAR ESTADISTICAS DEL VIDEO
	public void mostrarEstadisticas() {
		Video video;
		for (int i = 0; i < videos.size(); i++) {
			video = videos.get(i);
			System.out.println("Nombre del vídeo: " + "'" + video.getNombreVideo() + "'" + " creado en la fecha: "
					+ video.Formatofecha() + " con " + video.getLikes() + " likes y " + video.comentarios.size() + " comentarios.")  ;
		}
	}

	// FUNCION SALIR DEL MENU CANAL
	public void salir() {

		System.out.println("*SALIENDO*");
	}

}
