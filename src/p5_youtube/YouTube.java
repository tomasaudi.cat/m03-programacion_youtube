package p5_youtube;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class YouTube {
	static ArrayList<Canal> canales = new ArrayList<>();
	static Scanner leer = new Scanner(System.in);

	// FUNCION MAIN - ELEGIR OPCION MENU
	public static void main(String[] args) {
		int opcion;
		do {
			mostrarMenu();
			opcion = Integer.parseInt(leer.nextLine());

			switch (opcion) {

			// OPCION 1 - CREAR CANAL
			case 1:
				crearNuevoCanal();
				break;

			// OPCION 2 - SELECIONAR CANAL
			case 2:
				seleccionarCanal();
				break;

			// OPCION 3 - MOSTRAR ESTADISTICAS
			case 3:
				mostrarEstadisticas();
				break;

			// OPCION 4 - MOSTRAR ESTADISTICAS COMPLETAS
			case 4:
				mostrarEstadisticasCompletas();
				break;

			// OPCION 0 - SALIR DEL PROGRAMA
			case 0:
				System.out.println("*SALIENDO*\n");
				break;

			// OPCION INCORRECTA
			default:
				System.out.println("*Opción no válida* \nIntroduce una opción válida.");
			}
		} while (opcion != 0);
	}

	// FUNCION MENU
	private static void mostrarMenu() {
		System.out.println("--YouTube--");
		System.out.println("1- Nuevo canal");
		System.out.println("2- Seleccionar canal");
		System.out.println("3- Mostrar estadísticas");
		System.out.println("4- Mostrar estadísticas completas");
		System.out.println("0- Salir");
		System.out.println("---------------------------------");
		System.out.println("Introduce tu opción:");
	}

	// FUNCION OPCION 1 - CREAR NUEVO CANAL
	private static void crearNuevoCanal() {
		System.out.println("Introduce el nombre de tu canal:");
		String nombreCanal = leer.nextLine();
		// AÑADIR CANAL AL ARRAY LIST CANAL
		Canal nuevoCanal = new Canal (nombreCanal);
		canales.add(nuevoCanal);
		System.out.println("Canal creado.");
		nuevoCanal.getMenuCanal();
	}

	// FUNCION OPCION 2 - SELECCIONAR CANAL
	private static void seleccionarCanal() {
		if (canales.isEmpty()) {
			System.out.println("No hay canales disponibles.");
		} else {
			System.out.println("Seleccione un canal:\n");
			Canal canal;
			for (int i = 0; i < canales.size(); i++) {
				canal = canales.get(i);
				System.out.println(i + "- Nombre canal: " + "'" + canal.getnombreCanal() + "'" + " creado en fecha: "
						+ canal.formatoFecha());
			}

			int indice = leer.nextInt();
			leer.nextLine();

			if (indice >= 0 && indice < canales.size()) {

				canal = canales.get(indice);
				canal.getMenuCanal();

			} else {
				System.out.println("Índice no válido.");
			}
		}
	}

	// FUNCION OPCION 3 - MOSTRAR ESTADISTICAS
	private static void mostrarEstadisticas() {

		System.out.println("Canales creados: " + canales.size());
	}

	// FUNCION OPCION 4 - MOSTRAR ESTADISTICAS COMPLETAS
	private static void mostrarEstadisticasCompletas() {
		
		Video video;
		Canal canal;
		Comentario comentario;
		
		
		System.out.println("YouTube tiene: " + canales.size() + " canales creados.");

	    for (int i = 0; i < canales.size(); i++ ) {
	        System.out.println("- Nombre del canal: " + canal.getnombreCanal() + " creado en la fecha: " + canal.getFechaCreacion() + " con " + canal.videos.size() + " videos.");

	        for (int x = 0; x < canales.size(); x++ ) {
	            System.out.println("-- Video: " + video.getNombreVideo() + " en fecha " + video.getFechaCreacion() + " con " + video.getLikes() + " likes y " + video.comentarios.size() + " comentarios.");

	            for (int y = 0; y < canales.size(); y++ ) {
	                System.out.println("--- Comentario: " + comentario.getContenidoComentario() + " del usuario: " + comentario.getUsuario() + " en fecha: " + comentario.getFechaComentario() + ".");
	            }
	        }
	    }
	}
}
